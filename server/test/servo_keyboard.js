// require modules
const keypress = require('keypress');
const PanTilt = require('../servo.js');

const panTilt = new PanTilt();

// define movement step in percent
const STEP = 2;

let panValue = 50;
let tiltValue = 50;

// helper function limiting value to [0, 100] range
function constraint(value) {
    return Math.min(Math.max(value, 0), 100);
}
// make process.stdin start emitting keypress events 
keypress(process.stdin);
// handle keypress
process.stdin.on('keypress', (ch, key) => {
    switch (key.name) {
        case 'left':
            panValue = constraint(panValue + STEP);
            break;
        case 'right':
            panValue = constraint(panValue - STEP);
            break;
        case 'up':
            tiltValue = constraint(tiltValue - STEP);
            break;
        case 'down':
            tiltValue = constraint(tiltValue + STEP);
            break;
        case 'c':
            // ctrl+c will still stop the script, needs to be added manually because of raw mode
            if (key.ctrl) {
                process.stdin.pause();
            }
    }
    panTilt.rotate(panValue, tiltValue);
    console.log(`pan: ${panValue} tilt: ${tiltValue}`);
});

// use raw mode to enable character-by-character input
process.stdin.setRawMode(true);
// begin reading from stdin
process.stdin.resume();

console.log('Use keyboard arrows to control servos. Press ctrl+c to stop.');