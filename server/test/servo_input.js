// TEST RESULT:
// Pan:  500 - 1280 - 2000
// Tilt: 500 - 2300

const readline = require('readline');
const PanTilt = require('../servo.js');

const panTilt = new PanTilt();

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var option;
rl.question('Pan (1) or Tilt (2): ', (opt) => {
    option = opt;
    if (option != "1" && option != "2") {
        console.log('wrong option');
        rl.close();
    }
    else {
        recursiveAsyncReadLine();
    }
});

var recursiveAsyncReadLine = function () {
    rl.question('Value (%): ', (value) => {
        if (value == 'exit') {
            return rl.close();
        }
        if (value < 0 || value > 100) {
            console.log('Value must be between 0 and 100');
        }
        else {
            switch (option) {
                case "1":
                    panTilt.rotate(value, 50);
                    break;
                case "2":
                    panTilt.rotate(50, value);
                    break;
                default:
                    console.log('wrong option');
            }
        }
        recursiveAsyncReadLine();
    });
};
