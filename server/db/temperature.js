"use strict";

const db = require("./dbconnection");

let temperature = {
    getAllTemperatures: function (callback) {
        return db.query("SELECT * FROM temperatures ORDER BY time DESC", callback);
    },
    getTemperatureById: function (id, callback) {
        return db.query("SELECT * FROM temperatures WHERE id=?", [id], callback);
    },
    getTemperaturesByDate(from, to, callback) {
        return db.query("SELECT * FROM temperatures WHERE time BETWEEN ? AND ? ORDER BY time DESC", [from, to], callback);
    },
    addTemperature: function (temperature, callback) {
        return db.query("INSERT INTO temperatures (tempIn) VALUES(?)", [temperature.temp], callback);
    },
    deleteTemperature: function (id, callback) {
        return db.query("DELETE FROM temperatures WHERE id=?", [id], callback);
    },
    updateTemperature: function (id, temperature, callback) {
        return db.query("UPDATE temperatures SET tempIn=? WHERE id=?", [temperature.temp, id], callback);
    }
};

module.exports = temperature;