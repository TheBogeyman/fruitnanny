"use strict";

const mysql = require("mysql");
const settings = require("../config/dbsettings.json");

let connection = mysql.createPool(settings);

module.exports = connection;
