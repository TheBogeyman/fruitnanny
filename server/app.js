"use strict";

const config = require("../server/config/fruitnanny_config");

const express = require("express");
const app = express();
const server = require("http").createServer(app);
const io = require("socket.io")(server);
const cron = require("cron");

const panTilt = new (require('./utils/servo'))();
const custom_button = require("./routes/custom_button");
const dht = require("./routes/dht");
const light = require("./routes/light");
const tempModel = require("./db/temperature");
const temperature = require("./routes/temperature");

app.set("view engine", "ejs");
app.set("views", "views");
app.use("/public", express.static("public"));

app.get("/", (req, res, next) => {
    res.render("index", { config });
});
app.get("/settings", (req, res, next) => {
    res.render("settings", { config });
});

app.use("/api/light", light.default);
app.use("/api/dht", dht.default);
app.use("/api/custom_button", custom_button.default);
app.use("/api/temperature", temperature.default);

server.listen(7000, '0.0.0.0', () => {
    console.log('Listening on port %d', server.address().port);
    console.log('Started on %s', new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''));
});

// handle socket client connection
io.on('connection', (socket) => {
    var address = socket.request.connection.remoteAddress + ':' + socket.request.connection.remotePort;
    console.log('New connection from ' + address);
    socket.on('disconnect', () => {
        console.log('Client ' + address + ' disconnected');
    });

    socket.on('rotate', (x, y) => {
        console.log(`Rotate to x:${x.toFixed(1)} y:${y.toFixed(1)}`);
        panTilt.rotate(x, y);
        // send current pan/tilt head position to all connected clients
        socket.broadcast.emit('reset', x, y);
    });

    socket.on('stop', () => {
        console.log('Stop servo');
        panTilt.stop();
    });
    // send current pan/tilt head position to the new connected client
    const { x, y } = panTilt.getPosition();
    socket.emit('reset', x, y);
});

// log to DB every 30 minutes
new cron.CronJob('*/30 * * * *', function () {
    console.log('Logging data [%s]', new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''));
    logData();
}, null, true);

function logData() {
    tempModel.getAllTemperatures(function (err, result) {
        console.log(err ? err : result);
    });
}
