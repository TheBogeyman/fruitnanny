const helper = require("./helper");

let Gpio;
if (!helper.isWindows()) {
    // require modules
    const pigpio = require('pigpio');
    Gpio = pigpio.Gpio;
}
// define default servo configuration
const defaultConfig = {
    pan: {
        max: 500,
        min: 2000,
        pin: 2
    },
    tilt: {
        max: 2300,
        min: 500,
        pin: 3
    }
};

class PanTilt {
    constructor(config) {
        this.config = config || defaultConfig;
        if (!helper.isWindows()) {
            // initialize GPIOs
            this.pan = new Gpio(this.config.pan.pin, { mode: Gpio.OUTPUT });
            this.tilt = new Gpio(this.config.tilt.pin, { mode: Gpio.OUTPUT });
        }
        this.panPosition = 50;
        this.tiltPosition = 50;
        // center servos
        this.rotate(this.panPosition, this.tiltPosition);
        var that = this;
        setTimeout(function() { that.stop(); }, 500);
    }
    // return current pan/tilt head position
    getPosition() {
        // we cannot get the current position via getServoPulseWidth() anymore, because the stop method resets it,
        // so we simply remember it in 2 variables
        return {
            x: this.panPosition,
            y: this.tiltPosition
        };
    }
    // rotate pan/tilt head to the given position
    rotate(x, y) {
        // remember positions
        this.panPosition = x;
        this.tiltPosition = y;
        if (!helper.isWindows()) {
            this.pan.servoWrite(this.percentToPulse(x, this.config.pan.min, this.config.pan.max));
            this.tilt.servoWrite(this.percentToPulse(y, this.config.tilt.min, this.config.tilt.max));
        }
    }
    stop() {
        if (!helper.isWindows()) {
            this.pan.servoWrite(0);
            this.tilt.servoWrite(0);
        }
    }
    // convert 0-100% value to a pulse width in [min,max] range
    percentToPulse(value, min, max) {
        return Math.round(value * (max - min) / 100 + min);
    }
    // convert pulse width to 0-100% range
    pulseToPercent(value, min, max) {
        return (value - min) / (max - min) * 100;
    }
}

module.exports = PanTilt;