"use strict";

const express = require("express");
const tempModel = require("../db/temperature");
let router = express.Router();

router.get('/:id?', (req, res, next) => {
    if (req.params.id) {
        tempModel.getTemperatureById(req.params.id, function (err, rows) { res.json(err ? err : rows); });
    }
    else if (req.query.from && req.query.to) {
        tempModel.getTemperaturesByDate(req.query.from, req.query.to, function (err, rows) { res.json(err ? err : rows); });
    }
    else {
        tempModel.getAllTemperatures(function (err, rows) { res.json(err ? err : rows); });
    }
});

router.post('/', (req, res, next) => {
    tempModel.addTemperature(req.body, function (err, count) { res.json(err ? err : req.body); });
});

router.delete('/:id', (req, res, next) => {
    tempModel.deleteTemperature(req.params.id, function (err, count) { res.json(err ? err : count); });
});

router.put('/:id', (req, res, next) => {
    tempModel.updateTemperature(req.params.id, req.body, function (err, rows) { res.json(err ? err : rows); });
});

exports.default = router;
