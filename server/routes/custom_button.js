"use strict";

const express = require("express");
let router = express.Router();

router.get("/random", (req, res, next) => {
    let result = { random_number: Math.random() };
    res.json(result);
});

exports.default = router;
