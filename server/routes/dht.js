"use strict";

const express = require("express");
const cp = require("child_process");
let router = express.Router();

router.get("/current", (req, res, next) => {
    cp.exec("bin/dht22.py", (err, stdout, stderr) => {
        let t, h;
        if (stdout.length == 0) {
            t = 0;
            h = 0;
        }
        else {
            let values = stdout.split(" ");
            t = values[0];
            h = values[1];
        }
        let result = { humidity: h, temperature: t };
        res.json(result);
    });
});

exports.default = router;
