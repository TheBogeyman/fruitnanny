var dotEl = $('#pan-tilt-dot');

var isMoving = false;
var isConnected = false;

var socket = io();

socket.on('connect', function () {
    isConnected = true;
});

socket.on('disconnect', function () {
    isConnected = false;
});

socket.on('reset', function (x, y) {
    dotEl.css('left', x + '%');
    dotEl.css('top', y + '%');
});


if (window.PointerEvent) {
    dotEl.on('pointerdown', function(e) {
        if (!isConnected) {
            return;
        }
    
        dotEl.addClass('move');
        isMoving = true;
    });

    $(document).on('pointermove', function (event) {
        if (!isMoving) {
            return;
        }
    
        var rect = dotEl.parent()[0].getBoundingClientRect();
    
        // calculate dot position in relation to the box (in %)
        var x = (event.clientX - rect.left) / rect.width * 100;
        var y = (event.clientY - rect.top) / rect.height * 100;
    
        // limit values to 0-100 range
        x = x < 0 ? 0 : x > 100 ? 100 : x;
        y = y < 0 ? 0 : y > 100 ? 100 : y;
    
        // update dot position
        dotEl.css('left', x + '%');
        dotEl.css('top', y + '%');
    
        // send the position to the server
        socket.emit('rotate', x, y);
    });

    $(document).on('pointerup', function () {
        if (!isMoving) {
            return;
        }
    
        dotEl.removeClass('move');
        isMoving = false;
        
        // stop the servo to avoid flickering/buzzering
        socket.emit('stop');
    });
}
else {
    alert("PointerEvents not supported!");
}

function updatePanTiltHeight(isFullscreen) {
    var videoHeight = $("#video").height();
    if (isFullscreen) {
        var videoControlsHeight = $("#video-controls").height();
        videoHeight -= videoControlsHeight;
    }
    $("#pan-tilt-overlay").css('height', videoHeight);
}